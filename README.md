# hxDeploy

## Goals

#### Web service on hxdeploy.kla.bz (localhost:8800)
 * base62 token for each project
 * token <-> project data stored with haxelow
 * project creation => ? (manually for now)

#### Listen on github / gitlab webhooks
 * Clone repo (specific folder of said repo?) in a new folder (YYYYMMDDHHMMSS)
 * Execute compilation tasks (compass, haxe, npm tasks)
 * Cleanup unwanted files/dirs
 * Update symlink to prod

#### Allow for rollbacks, etc.
 * Manually first, then with back office (listing) and webservice

#### Project config: `hxdeploy.json`
TODO: stop previous & start current
```
{
	"tasks": [
		"css",
		"haxe-front",
		"haxe-serviceworker"
	],
	"unwanted": [
		"**/node_modules",
		"./package.json"
	]
}
```
