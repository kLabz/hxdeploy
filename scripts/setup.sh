#/bin/bash

# Haxelibs from git
haxelib git abe https://github.com/abedev/abe.git
haxelib git js-kit https://github.com/clemos/haxe-js-kit.git

# Other haxelibs
haxelib install haxelow
haxelib install hxnodejs
