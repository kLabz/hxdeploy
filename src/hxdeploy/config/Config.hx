package hxdeploy.config;

import haxe.Json;
import haxe.Resource;

class Config {
	public static var instance(get, null):Config;

	private static function get_instance():Config {
		if (instance == null) {
			instance = Json.parse(Resource.getString("config"));
		}

		return instance;
	}

	public var port(default, null):Int;
	public var path(default, null):String;
	public var logsPath(default, null):String;
}
