package hxdeploy;

import abe.App;
import hxdeploy.config.Config;
import hxdeploy.route.GitHook;
import hxdeploy.utils.Log;

class HxDeploy implements abe.IRoute {
	public static var app(default, null):App;

	static public function main() {
		app = new App();

		app.use(function (req, res, next) {
			// Website you wish to allow to connect
			res.setHeader('Access-Control-Allow-Origin', '*');

			// Request methods you wish to allow
			res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');

			// Request headers you wish to allow
			res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

			// Set to true if you need the website to include cookies in the requests sent
			// to the API (e.g. in case you use sessions)
			// res.setHeader('Access-Control-Allow-Credentials', "true");

			// Pass to next layer of middleware
			next();
		});

		app.router.register(new GitHook());

		var port = Config.instance.port;
		app.http(port);
		Log.info('HxDeploy running on port $port');
		Log.log('Waiting for github/gitlab webhooks...\n');
	}

}
