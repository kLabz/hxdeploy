package hxdeploy.utils;

import haxe.Json;
import haxe.extern.EitherType;
import js.Error;
import js.node.Buffer;
import js.node.ChildProcess;
import js.node.Fs;
import sys.FileSystem;
import sys.io.File as SysFile;
import sys.io.FileOutput;

import express.Response;
import hxdeploy.config.Config;
import hxdeploy.model.Project;
import hxdeploy.model.HxDeployConfig;

typedef DeployTask = Void->Void;

class Deploy {
	public static inline var CODE_OK:Int = 200;
	public static inline var CODE_ERROR:Int = 406;
	public static inline var CODE_DUPLICATE:Int = 409;

	// Store some paths
	private var _logFile:String;
	private var _projectRoot:String;
	private var _releaseFolder:String;
	private var _releasePath:String;
	private var _rootPath:String;

	// Project/release data
	private var _project:Project;
	private var _config:HxDeployConfig;
	private var _commitHash:String;
	private var _nextCallback:Int->Void;

	// Deploy data
	private var _tasks:Array<DeployTask>;

	public function new(project:Project, nextCallback:Int->Void) {
		_project = project;
		_nextCallback = nextCallback;

		_projectRoot = Config.instance.path + "/" + project.shortName;
		_releaseFolder = DateTools.format(Date.now(), "%Y%m%d%H%M") + "00";
		// _releaseFolder = DateTools.format(Date.now(), "%Y%m%d%H%M%S").substr(0, 13) + "0";
		_releasePath = _projectRoot + "/" + _releaseFolder;

		if (FileSystem.exists(_releasePath)) {
			Log.warn('Last release was less than a minute ago', log);
			_nextCallback(CODE_DUPLICATE);

		} else {

			var logsPath = Config.instance.logsPath + "/" + project.shortName;
			_logFile = logsPath + "/" + _releaseFolder + ".log";
			if (!FileSystem.exists(logsPath)) {
				FileSystem.createDirectory(logsPath);
			}

			_tasks = [
				initHxDeployProject,
				cloneProject,
				getCommit,
				checkCommit,
				parseConfig,
				runNpmInstall,
				runHaxelibInstall,
				runNpmTasks,
				linkPersistentFiles,
				removeUnwantedFiles,
				registerCommit,
				stopPreviousRelease,
				updateReleaseLink,
				startRelease
			];

			nextTask();
		}
	}

	private function log(message:String):Void {
		Fs.appendFileSync(_logFile, message + "\n", {
			flag: "a"
		});
	}

	private function errorCallback():Void {
		if (_nextCallback != null) {
			_nextCallback(CODE_ERROR);
		}
	}

	private function nextTask():Void {
		if (_tasks.length > 0) {
			var task = _tasks.shift();
			task();
		} else {
			Log.info('Project ${_project.fullName} deployed.', log);

			if (_nextCallback != null) {
				_nextCallback(CODE_OK);
			}
		}
	}

	private function command(
		cmd:String,
		cwd:String,
		callback:Null<ChildProcessExecError>->EitherType<Buffer,String>->Void
	):Void {
		ChildProcess.exec(
			cmd, {cwd: cwd},
			function(err:Null<ChildProcessExecError>, out:EitherType<Buffer,String>, _:EitherType<Buffer,String>) {
				callback(err, out);
			}
		);
	}

	private function initHxDeployProject():Void {
		if (!FileSystem.exists(_projectRoot)) {
			Log.info('Creating project folders...', log);

			FileSystem.createDirectory(_projectRoot);
			FileSystem.createDirectory(_projectRoot + "/.shared");
			FileSystem.createDirectory(_projectRoot + "/.commits");
		} else {
			Log.log("Found project folders", log);
		}

		nextTask();
	}

	private function cloneProject():Void {
		Log.info('Cloning project into ${_releaseFolder}...', log);

		command('git clone --depth 1 ${_project.remote} ${_releaseFolder}', _projectRoot, function(err, out) {
			if (out != null && out != "") log(out);

			if (err != null && err.code > 0) {
				log("");
				Log.error(err.signal, log);
				errorCallback();

			} else {
				nextTask();
			}
		});
	}

	private function getCommit():Void {
		command('git rev-parse --verify HEAD', _releasePath, function(err, out) {
			_commitHash = StringTools.trim(out);

			if (err != null && err.code > 0) {
				var msg = err.signal;
				if (msg == null || msg == "") msg = 'An error occured while getting last commit hash';

				log("");
				Log.error(msg, log);
				errorCallback();

			} else {
				nextTask();
			}
		});
	}

	private function checkCommit():Void {
		try {
			command(
				'ls -Al1 ${FileSystem.fullPath(_projectRoot + "/.commits/" + _commitHash)}',
				_releasePath,
				function(err, out) {
					if (err != null && err.code > 0) {
						nextTask();

					} else {
						Log.warn('This commit has already been deployed', log);
						_nextCallback(CODE_DUPLICATE);
					}
				}
			);
		} catch (_:Dynamic) {
			nextTask();
		}
	}

	private function parseConfig():Void {
		Sys.setCwd(_releasePath);
		var configPath:String = _releasePath + "/hxdeploy.json";

		if (FileSystem.exists(configPath)) {
			Log.log('Found hxdeploy config', log);

			try {
				_config = cast Json.parse(SysFile.getContent(configPath));

				if (_config.root != null) {
					if (FileSystem.exists(_config.root)) {
						var fullPath:String = FileSystem.fullPath(_config.root);

						if (StringTools.startsWith(fullPath, _releasePath + "/")) {
							_rootPath = "/" + _config.root;
							_releasePath += _rootPath;
						}
					}
				}

				nextTask();
			} catch (e:Dynamic) {
				Log.error('Error while parsing hxdeploy config file', log);
				Log.error(e, log);
				errorCallback();
			}

		} else {
			Log.warn('No hxdeploy config found', log);
			nextTask();
		}
	}

	private function runNpmInstall():Void {
		Log.info('Running npm install...', log);

		command('yarn install', _releasePath, function(err, out) {
			if (out != null && out != "") log(out);

			if (err != null && err.code > 0) {
				Log.error('Npm install failed', log);
				if (err.signal != null && err.signal != "") {
					Log.error(err.signal, log);
				}
				errorCallback();

			} else {
				nextTask();
			}
		});
	}

	private function runHaxelibInstall():Void {
		if (_config.haxelibs == null || _config.haxelibs.length == 0) {
			return nextTask();
		}

		Log.info('Installing haxelibs...', log);
		var libs = _config.haxelibs.copy();

		function nextLib() {
			if (libs.length > 0) {
				var haxelib = libs.shift();

				if (haxelib.length > 1) {
					switch (haxelib[0]) {
						case "GIT":
							if (haxelib.length == 3) {
								command("haxelib --always dev " + haxelib[1], _releasePath, function(err, out) {
									if (out != null && out != "") log(out);

									if (err != null && err.code > 0) {
										Log.error('Haxelib installation error', log);
										if (err.signal != null && err.signal != "") {
											Log.error(err.signal, log);
										}

										errorCallback();
									} else {
										command("haxelib --always git " + haxelib[1] + " " + haxelib[2], _releasePath, function(err, out) {
											if (out != null && out != "") log(out);

											if (err != null && err.code > 0) {
												Log.error('Haxelib installation error', log);
												if (err.signal != null && err.signal != "") {
													Log.error(err.signal, log);
												}

												errorCallback();
											} else {
												command("haxelib --always set " + haxelib[1] + " git", _releasePath, function(err, out) {
													if (out != null && out != "") log(out);

													if (err != null && err.code > 0) {
														Log.error('Haxelib installation error', log);
														if (err.signal != null && err.signal != "") {
															Log.error(err.signal, log);
														}

														errorCallback();
													} else {
														nextLib();
													}
												});
											}
										});
									}
								});
							}

						case "LIB":
							if (haxelib.length == 2) {
								command("haxelib --always install " + haxelib[1], _releasePath, function(err, out) {
									if (out != null && out != "") log(out);

									if (err != null && err.code > 0) {
										Log.error('Haxelib installation error', log);
										if (err.signal != null && err.signal != "") {
											Log.error(err.signal, log);
										}

										errorCallback();
									} else {
										nextLib();
									}
								});
							} else if (haxelib.length == 3) {

								command("haxelib --always dev " + haxelib[1], _releasePath, function(err, out) {
									if (out != null && out != "") log(out);

									if (err != null && err.code > 0) {
										Log.error('Haxelib installation error', log);
										if (err.signal != null && err.signal != "") {
											Log.error(err.signal, log);
										}

										errorCallback();
									} else {
										command("haxelib --always install " + haxelib[1] + " " + haxelib[2], _releasePath, function(err, out) {
											if (out != null && out != "") log(out);

											if (err != null && err.code > 0) {
												Log.error('Haxelib installation error', log);
												if (err.signal != null && err.signal != "") {
													Log.error(err.signal, log);
												}

												errorCallback();
											} else {
												command("haxelib --always set " + haxelib[1] + " " + haxelib[2], _releasePath, function(err, out) {
													if (out != null && out != "") log(out);

													if (err != null && err.code > 0) {
														Log.error('Haxelib installation error', log);
														if (err.signal != null && err.signal != "") {
															Log.error(err.signal, log);
														}

														errorCallback();
													} else {
														nextLib();
													}
												});
											}
										});
									}
								});
							}
					}
				} else {
					Log.error('Invalid haxelib definition', log);
					errorCallback();
				}

			} else {
				nextTask();
			}
		}

		nextLib();
	}

	private function runNpmTasks():Void {
		if (_config.tasks == null || _config.tasks.length == 0) {
			return nextTask();
		}

		Log.info('Running npm tasks...', log);
		var tasks = _config.tasks.copy();

		function nextNpmTask() {
			if (tasks.length > 0) {
				var task = tasks.shift();

				Log.log("Npm task: " + task, log);

				command("npm run " + task, _releasePath, function(err, out) {
					if (out != null && out != "") log(out);

					if (err != null && err.code > 0) {
						Log.error('Npm error', log);
						if (err.signal != null && err.signal != "") {
							Log.error(err.signal, log);
						}

						errorCallback();
					} else {
						nextNpmTask();
					}
				});
			} else {
				nextTask();
			}
		}

		nextNpmTask();
	}

	private function linkPersistentFiles():Void {
		if (_config.persistent == null || _config.persistent.length == 0) {
 			Log.log('No persistent files to link', log);
			return nextTask();
		}

		Log.info('Link persistent files...', log);

		if (!FileSystem.exists(_projectRoot + "/.shared")) {
			FileSystem.createDirectory(_projectRoot + "/.shared");
		}

		var files = _config.persistent.copy();

		function nextFile() {
			if (files.length > 0) {
				var file = files.shift();

				var path = _rootPath + "/" + file;
				path = path.substr(0, path.lastIndexOf("/"));

				if (!FileSystem.exists(_projectRoot + "/" + _releaseFolder + "/" + path)) {
					FileSystem.createDirectory(_projectRoot + "/" + _releaseFolder + "/" + path);
				}

				command(
					"ln -s " +
					FileSystem.fullPath(_projectRoot + "/.shared/" + _rootPath + "/" + file) +
					" " +
					FileSystem.fullPath(_projectRoot + "/" + _releaseFolder + path + "/"),
					_releasePath,
					function(err, out) {
						if (err != null && err.code > 0) {
							Log.error('Error linking $file', log);
							if (err.signal != null && err.signal != "") {
								Log.error(err.signal, log);
							}

							errorCallback();
						} else {
							Log.log('Linked $file', log);
							nextFile();
						}
					}
				);

			} else {
				nextTask();
			}
		}

		nextFile();
	}

	private function removeUnwantedFiles():Void {
		if (_config.unwanted == null || _config.unwanted.length == 0) {
 			Log.log('No unwanted files to remove', log);
			return nextTask();
		}

		Log.info('Removing unwanted files...', log);
		Sys.setCwd(_releasePath);

		for (unwanted in _config.unwanted) {
			if (FileSystem.exists(unwanted)) {
				var fullPath:String = FileSystem.fullPath(unwanted);

				if (StringTools.startsWith(fullPath, _projectRoot + "/" + _releaseFolder + "/")) {
					ChildProcess.execSync("rm -rf " + fullPath, {cwd: _releasePath});
					Log.log('Removed $unwanted', log);

				} else {
					Log.error('Error: Trying to remove file/directory out of project boundaries: $unwanted', log);
					errorCallback();
				}
			} else {
				Log.warn('Warning: File/directory not found: $unwanted', log);
			}
		}

		nextTask();
	}

	private function registerCommit():Void {
		command('ln -s ../$_releaseFolder ../.commits/$_commitHash', _projectRoot + "/" + _releaseFolder, function(err, out) {
			if (err != null && err.code > 0) {
				Log.error('Error registering commit', log);
				if (err.signal != null && err.signal != "") {
					Log.error(err.signal, log);
				}

				errorCallback();
			} else {
				nextTask();
			}
		});
	}

	private function stopPreviousRelease():Void {
		if (FileSystem.exists(_projectRoot + "/.current")) {
			Log.info('Stopping previous release...', log);

			var stopTask:String = "stop";
			if (_config.stop != null) stopTask = _config.stop;

			command('npm run $stopTask', _projectRoot + "/.current" + _rootPath, function(err, out) {
				if (err != null && err.code > 0) {
					Log.error('Error stopping previous release', log);
					if (err.signal != null && err.signal != "") {
						Log.error(err.signal, log);
					}

				}

				FileSystem.deleteFile(_projectRoot + "/.current");

				nextTask();
			});
		} else {
			Log.log('No previous release found', log);
			nextTask();
		}
	}

	private function updateReleaseLink():Void {
		Log.info('Changing symlink to new release...', log);

		command('ln -s $_releaseFolder .current', _projectRoot, function(err, out) {
			if (err != null && err.code > 0) {
				Log.error('Error linking release', log);
				if (err.signal != null && err.signal != "") {
					Log.error(err.signal, log);
				}

				errorCallback();
			} else {
 				nextTask();
 			}
		});
	}

	private function startRelease():Void {
		Log.info('Launching new release...', log);

		var startTask:String = "start";
		if (_config.start != null) startTask = _config.start;

		command('npm run $startTask', _releasePath, function(err, out) {
			if (err != null && err.code > 0) {
				Log.error('Error starting release', log);
				if (err.signal != null && err.signal != "") {
					Log.error(err.signal, log);
				}

				errorCallback();
			} else {
 				nextTask();
 			}
		});
	}

}
