package hxdeploy.utils;

import js.Node;

class Log {
	public static var RED:Int = 31;
	public static var YELLOW:Int = 33;
	public static var BLUE:Int = 34;
	public static var WHITE:Int = 37;
	public static var DEFAULT_COLOR:Int = WHITE;

	public static function trace(message:String, ?level:LogLevel):Void {
		if (level == null) level = LOG;

		var fx = switch(level) {
			case LOG: log;
			case INFO: info;
			case WARN: warn;
			case ERROR: error;
		};

		fx(message);
	}

	public static function log(message:String, ?writeCallback:String->Void):Void {
		if (writeCallback == null) writeCallback = cast Node.console.log;
		writeCallback(getColorCode(WHITE) + getDate() + message + getColorCode(DEFAULT_COLOR));
	}

	public static function info(message:String, ?writeCallback:String->Void):Void {
		if (writeCallback == null) writeCallback = cast Node.console.info;
		writeCallback(getColorCode(BLUE) + getDate() + message + getColorCode(DEFAULT_COLOR));
	}

	public static function warn(message:String, ?writeCallback:String->Void):Void {
		if (writeCallback == null) writeCallback = cast Node.console.warn;
		writeCallback(getColorCode(YELLOW) + getDate() + message + getColorCode(DEFAULT_COLOR));
	}

	public static function error(message:String, ?writeCallback:String->Void):Void {
		if (writeCallback == null) writeCallback = cast Node.console.error;
		writeCallback(getColorCode(RED) + getDate() + message + getColorCode(DEFAULT_COLOR));
	}

	private static function getColorCode(color:Int):String {
		return "\u001B[" + color + "m";
	}

	private static function getDate():String {
		return "[" + DateTools.format(Date.now(), "%Y-%m-%d %H:%M:%S") + "] ";
	}
}

enum LogLevel {
	LOG;
	INFO;
	WARN;
	ERROR;
}
