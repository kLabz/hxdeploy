package hxdeploy.model;

typedef Token = {
	var left:String;
	var right:String;
}

class Project {
	private static var db(get, null):HaxeLow;

	private static function get_db():HaxeLow {
		if (db == null) {
			db = new HaxeLow("tokens.json");
		}

		return db;
	}

	public var tokenLeft(default, null):String;
	public var tokenRight(default, null):String;

	public var remote(default, null):String;
	public var fullName(default, null):String;
	public var shortName(default, null):String;

	public static function get(left:String, right:String):Project {
		var projects = db.col(Project).filter(function(project) {
			var token = getToken(project);
			return (token.left == left && token.right == right);
		});

		return projects.shift();
	}

	public static function getToken(project:Project):Token {
		return {
			left: project.tokenLeft,
			right: project.tokenRight
		};
	}
}
