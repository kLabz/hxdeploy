package hxdeploy.model;

typedef HxDeployConfig = {
	@:optional var start:String;
	@:optional var stop:String;
	@:optional var root:String;

	@:optional var tasks:Array<String>;
	@:optional var unwanted:Array<String>;
	@:optional var persistent:Array<String>;
	@:optional var haxelibs:Array<Array<String>>;
}
