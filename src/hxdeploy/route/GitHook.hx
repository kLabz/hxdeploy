package hxdeploy.route;

import sys.FileSystem;

import abe.IRoute;
import express.Response;
import hxdeploy.config.Config;
import hxdeploy.model.Project;
import hxdeploy.utils.Deploy;
import hxdeploy.utils.Log;

@:path("/hook/")
class GitHook implements IRoute {
	@:get("/")
	public function index():Void {
		response.send("Hello Gitlab!");
	}

	#if DEV
	@:get("/test/:tokenLeft/:tokenRight")
	public function test(tokenLeft:String, tokenRight:String):Void {
		pushHook(response, tokenLeft, tokenRight);
	}
	#end

	@:post("/gitlab")
	public function gitlab():Void {
		var action:String = request.headers.get("x-gitlab-event");
		var token:String = request.headers.get("x-gitlab-token");

		if (action == "Push Hook" && token != null && token.split("/").length == 2) {
			var tokens:Array<String> = token.split("/");
			pushHook(response, tokens[0], tokens[1]);
		} else {
			if (action != "Push Hook") {
				log(null, "Unknown hook action: " + action, ERROR);
				response.send("Unknown hook action: " + action);
			} else {
				log(null, "Unknown token", ERROR);
				response.send("Unknown token");
			}
		}
	}

	@:post("/github")
	public function github():Void {
		var data = "";

		request.on("data", function(chunk) {
			data += chunk;
		});

		request.on("end", function() {
			try {
				var json = haxe.Json.parse(data);
				var action = json.hook.events[0];

				if (action != "push") {
					log(null, "Unknown hook action: " + action, ERROR);
					response.send("Unknown hook action: " + action);
				} else {
					var token = json.hook.config.secret;
					var tokens:Array<String> = token.split("/");

					pushHook(response, tokens[0], tokens[1]);
				}
			} catch (e:Dynamic) {
				log(null, "Invalid github hook call", ERROR);
				response.send("Invalid github hook call");
			}
		});
	}

	private function pushHook(response:Response, tokenLeft:String, tokenRight:String):Void {
		var project = Project.get(tokenLeft, tokenRight);

		if (project != null) {
			log(project, "Hook for project: " + project.fullName);

			new Deploy(project, function(returnCode:Int) {
				switch (returnCode) {
					case 200:
					response.send("OK");

					case 406:
					response.sendStatus(406);
					response.send("Deploy error");

					case 409:
					response.sendStatus(200);
					response.send("Already deployed this commit");
				}
			});

		} else {
			log(project, "Unknown token", ERROR);
			response.send("Unknown token");
		}
	}

	private function log(project:Project, message:String, ?logLevel:LogLevel):Void {
		Log.trace([
			project == null ? "Unknown project" : project.shortName,
			": ",
			message
		].join(""), logLevel);
	}
}
